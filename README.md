We would like you to create a simple IOS application using your preferred language of choice (Swift or Obj-c), either is fine.

The application should:

- Show a list of match events (a title/thumbnail for each). Brownie points for making the video play when thumbnail is tapped.
- Allow the user to record a video using their phones camera to the local device as a physical file and add it to the list. Brownie points for tidying up after yourself and deleting videos off the device once uploaded etc...

User interface is not important, but brownie points for any effort made here, a UITableView will suffice.

**List of match events**

The list of match events and associated video will be loaded from the Pitchero API. All calls to the Pitchero API should have an Authorization header token with the value:


```
Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIyNDM4NDgyIiwiaXNzIjoiaHR0cDpcL1wvYXBpLnBpdGNoZXJvLmNvbVwvdjJcL2F1dGhcL2p3dCIsImlhdCI6MTQ5MDM1Mjg0MSwiZXhwIjoxNDkwOTU0MDQxLCJuYmYiOjE0OTAzNTI4NDEsImp0aSI6ImQ1MDA3Y2YxMGY5ZTU2OGFmYjE2NTdlMjY2ODZkMWJjIn0.gNfUB-vlaJnnt2NaZLicY-2QdYGZWq6GNcqzEg4-f4I
```


The api accepts and responds with JSON Content-Type: application/json.

It can be found here:

http://api.pitchero.com/v2/

Documentation for the api call is here:

[http://api.pitchero.com/v2/docs/fixtures.html#match-player-events-fixture-match-player-events-get](Link URL)

```The fixture id is: 0-3651243
The team id is: 172866```

*Amazon S3*

To upload videos you should use the AWS S3 IOS SDK:

[http://docs.aws.amazon.com/mobile/sdkforios/developerguide/s3-simple-storage-service-for-ios.html](Link URL)

The details you need to use this SDK are:


```
"bucket": "mobile-tech-test-pitchero-com",
"access_key_id": "AKIAI2ZCODYHJA6X3EVQ",
"access_key_secret": "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"
```


**Create Video Record**

Once a video has successfully uploaded to Amazon S3 bucket you should call the Pitchero API to tell us about the video:

[http://api.pitchero.com/v2/docs/fixtures.html#videos](Link URL)

This will return a *video_id*.

**Match Event**

You should then create a match event using the following JSON body in the request:

[http://api.pitchero.com/v2/docs/fixtures.html#match-player-events-match-player-events-post](Link URL)


```
#!json

{
  "event_type_id": 2,
  "fixture_id": "0-3651243",
  "member_id": 2957873,
  "team_id": 172866,
  "second": $SECONDS,
  "period": 1
}

```

Where $SECONDS is the time of the events in seconds. I suggest you create a timer when the app opens and then send the current value of the timer as the seconds attribute, when you call this api method.

This will return a *player_event_id*.

**Attach video to match event**

Then finally you should attach the video to the event using the *video_id* you received earlier and the *player_event_id*:

[http://api.pitchero.com/v2/docs/fixtures.html#match-player-events-event-videos-post](Link URL)

Once this process of api calls is complete the newly created match event should appear in the list and will have a video attached.

I have been as vague as possible purposely, to show you can use your initiative, read documentation and investigate our api and how it works yourself. You should have all the details you need to achieve the above requirements.